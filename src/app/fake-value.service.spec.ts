import { FakeValueService } from './fake-value.service';

describe('FakeValueService', () => {
  let service;
  beforeEach(() => {
    service = new FakeValueService();
  });

  it('#getValue should return faked service value', () => {
    expect(service.getValue()).toBe('faked service value');
  });

  it('#getValue should return faked observable service value', 
    (done: Function) => {
      service.getObservableValue().subscribe(value => {
        expect(value).toBe('faked observable service value');
	done();
      });
    });

  it('#getValue should return faked promise service value', 
    (done: Function) => {
      service.getPromiseValue().then(value => {
        expect(value).toBe('faked promise service value');
	done();
      });
    });
});
