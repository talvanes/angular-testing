import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ValueService } from './value.service';

@Injectable({
  providedIn: 'root'
})
export class FakeValueService extends ValueService {

  constructor() {
    super();
  }

  getValue() : string {
    return 'faked service value';
  }

  getObservableValue() : Observable<String> {
    return of('faked observable service value');
  }

  getPromiseValue() : Promise<String> {
    return new Promise((resolve, reject) => {
      resolve('faked promise service value');
    });
  }
}
