import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValueService {

  constructor() { }


  getValue() : String {
    return 'real value';
  }

  getObservableValue() : Observable<String> {
    return of('observable value');
  }

  getPromiseValue() : Promise<String> {
    //return Promise.resolve('promise value');

    return new Promise((resolve, reject) => {
      resolve('promise value');
    });
  }
}
